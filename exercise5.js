var myBuffer = [];
process.stdin.on('data', function (content) {
    var buffer = new Buffer(content);
    myBuffer.push(buffer);
});
process.stdin.on('end', function () {
    var resultantBuffer = Buffer.concat(myBuffer);
    console.log(resultantBuffer);
});